/*fetch('https://jsonplaceholder.typicode.com/todos')

console.log(fetch('https://jsonplaceholder.typicode.com/todos'))

*/


//number 3

fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json)=> console.log(json))


//number 4

fetch('https://jsonplaceholder.typicode.com/todos')
      .then(response => response.json())
      .then(tasks => tasks.map(task => task.title))
      .then(titles => console.log(titles))



//number 5
fetch('https://jsonplaceholder.typicode.com/todos/2')
.then((response) => response.json())
.then((json)=> console.log(json))





//number 6
fetch('https://jsonplaceholder.typicode.com/todos/2')
      .then(response => response.json())
      .then(task => {
        console.log(`${task.title}, ${task.completed}`);
      });



//number 7

fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		userId: 303,
		title:'s28 new 303',
		completed: 'true',
		
	})
})

.then((response) => response.json())
.then((json) => console.log(json))



//number 8

fetch('https://jsonplaceholder.typicode.com/todos/8', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		id: 8,
		title:'updated new 303',
		completed: 'true',
		userId: 8,
	})
})

.then((response) => response.json())
.then((json) => console.log(json))

//number 9


fetch('https://jsonplaceholder.typicode.com/todos/10', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		id: 10,
		title:'updated new 303',
		description: 'describe here',
		status: 'true',
		dateCompleted: 'October 25, 2021',
		userId: 10,
	})
})

.then((response) => response.json())
.then((json) => console.log(json))


//number 10

fetch('https://jsonplaceholder.typicode.com/todos/30',{
		method: 'PATCH',
		headers: {
			'Content-Type' : 'application/json'
		},
		body: JSON.stringify({
			id: 88,
			title:'updated new 808',
			completed: 'true',
			userId: 88,
		})
	})
	.then((response) => response.json())
	.then((json) => console.log(json));



//number 11


fetch('https://jsonplaceholder.typicode.com/todos/10', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		id: 20,
		title:'updated new another 2020202',
		completed: 'true',
		dateCompleted: 'September 21, 2021',
		userId: 20
	})
})

.then((response) => response.json())
.then((json) => console.log(json))